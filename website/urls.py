from django.conf.urls import url
from . import views
from django.contrib.auth.views import(
	LoginView,
	LogoutView,
	PasswordResetView,
	PasswordResetConfirmView,
	PasswordResetCompleteView
	)
from django.contrib.auth import views as auth_views


urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'register/$', views.register, name='register'),
	url(r'profile/(?P<username>[a-zA-Z0-9]+)/$', views.profile, name='profile'),
	url(r'edit_profile/$', views.edit_profile, name='edit_profile'),
	url(r'change-password/$', views.change_password, name='change-password'),
	url(r'login/recover/$', auth_views.PasswordResetView.as_view(template_name = 'login/password_reset_form.html'), 
		name='password_reset'),
	url(r'logout/$', auth_views.LogoutView.as_view(), name='logout'),
	url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
	url('login/password-reset/done', auth_views.PasswordResetDoneView.as_view(template_name = 'login/password_reset_done.html'),
		name = 'password_reset_done' ),   
	url(r'login/password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
		auth_views.PasswordResetConfirmView.as_view(template_name = 'login/password_reset_new.html'), name = 'password_reset_confirm' ),      
	url('login/password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name = 'login/password_reset_complete.html'),
		name='password_reset_complete'),
]
