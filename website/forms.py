from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import LSUser
from django.forms import ModelForm


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    username = forms.CharField(max_length=100, help_text='Required')
    photo = forms.ImageField(help_text='Required')
    mobile = forms.CharField(max_length=100, required=False, help_text='Optional')
    about = forms.CharField(widget=forms.Textarea, required=False, help_text='Optional')
    state = forms.CharField(max_length=100, required=False, help_text='Optional')
    city = forms.CharField(max_length=100, required=False, help_text='Optional')
    lga = forms.CharField(max_length=100, required=False, help_text='Optional')
    street = forms.CharField(max_length=100, required=False, help_text='Optional')
    gender = forms.CharField(max_length=100, required=True, help_text='Optional')

    class Meta:
        model = LSUser
        fields = ('username', 'first_name', 'last_name', 'email', 'photo', 'mobile',
        		 'about', 'state', 'city', 'lga', 'street', 'gender', 'password1', 'password2', )


class ProfileForm(ModelForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    photo = forms.ImageField(help_text='Required')
    mobile = forms.CharField(max_length=100, required=False, help_text='Optional')
    about = forms.CharField(widget=forms.Textarea, required=False, help_text='Optional')
    state = forms.CharField(max_length=100, required=False, help_text='Optional')
    city = forms.CharField(max_length=100, required=False, help_text='Optional')
    lga = forms.CharField(max_length=100, required=False, help_text='Optional')
    street = forms.CharField(max_length=100, required=False, help_text='Optional')
    gender = forms.CharField(max_length=100, required=True, help_text='Optional')
    class Meta:
    	model = LSUser
    	fields = ('email', 'first_name', 'last_name', 'photo', 'mobile', 'gender', 'about', 'street', 'city', 'state', 'lga',) #Note that we didn't mention user field here.


# class PasswordChangeForm(ModelForm):
# 	password = forms.CharField()
# 	model = LSUser
# 	fields = ('password', 'password1', 'password2')
