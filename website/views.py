from django.shortcuts import render, redirect, get_object_or_404
import datetime
from datetime import date, timedelta, datetime
from django.utils.timesince import timesince
from .forms import *
from django.contrib.auth.forms import PasswordChangeForm
import random
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.views import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import login, authenticate, update_session_auth_hash


def home(request):
	return render(request, 'home.html')


def login(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		return render(request, 'login.html')


def register(request):
	if request.method == 'POST':
		form = SignUpForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			# username = form.cleaned_data.get('email')
			# password = form.cleaned_data.get('password1')
			# user = authenticate(email=username, password=password)
			# login(request, user)
			return redirect('login')
	else:
		form = SignUpForm()
	return render(request, 'register.html', {'form': form})


def profile(request, username):
	user = LSUser.objects.get(username=username)
	return render(request, 'profile.html', {"user":user})


@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=request.user)
        # profile_form = ProfileForm(request.POST, request.FILES, instance=request.user)  # request.FILES is show the selected image or file
        if form.is_valid():
            user_form = form.save()
            form.save()
            return redirect('profile', request.user.username)
    else:
        form = ProfileForm(instance=request.user)
        # profile_form = ProfileForm(instance=request.user)
        args = {}
        # args.update(csrf(request))
        args['form'] = form
        # args['profile_form'] = profile_form
        return render(request, 'edit_profile.html', args)

@login_required
def recover(request):
	return render(request, 'recover.html')

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            # messages.success(request, _('Your password was successfully updated!'))
            return redirect('profile', request.user.username)
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })

@login_required
def logout(request):
    try:
        del request.session['username']
        return redirect('login')
    except:
     pass
    return render(request, 'login.html', {})
